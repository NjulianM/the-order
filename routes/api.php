<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\ProductoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('productos', 'App\Http\Controllers\ProductoController@index');
Route::get('productos/{producto}', 'App\Http\Controllers\ProductoController@show');
Route::post('productos/{user}', 'App\Http\Controllers\ProductoController@store');
Route::put('productos/{producto}', 'App\Http\Controllers\ProductoController@update');

Route::post('pedidos', 'App\Http\Controllers\PedidoController@store');
Route::get('pedidos', 'App\Http\Controllers\PedidoController@index');

Route::post('pedidos/{pedido}/productos/{producto}', 'App\Http\Controllers\PedidoProductoController@store');
Route::get('pedidos_productos', 'App\Http\Controllers\PedidoProductoController@index');

Route::post('users/register', 'App\Http\Controllers\UserController@store');//registrar un nuevo foodtruck
Route::post('users/login', 'App\Http\Controllers\UserController@login');//ingresar a los paneles de foodtruck 
Route::get('users/auth', 'App\Http\Controllers\UserController@auth');//devuelve el usuario o foodtruck autenticado



// Route::get('productos', [ProductoController::class,'index']);
