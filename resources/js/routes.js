import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

 const router = new Router({
    routes: [
        // {
        //     path: '*',
        //     redirect: '/login'
        // },
        // {
        //     path: '/home',
        //     name: 'home',
        //     component: require('./views/Home.vue').default,
            
        // },
        // {
        //     path: ':idr',
        //     name: 'post',
        //     component: require('./views/post.vue').default,
        //     props: true,
        //     // meta: {
        //     //     autenticado: true
        //     // }
        // },
        // {
        //     path: '/login',
        //     name: 'login',
        //     component: require('./components/LoginComponent.vue').default
        // },
        // {
        //     path: '/register',
        //     name: 'register',
        //     component: require('./components/RegisterComponent.vue').default
        // }
        {
            path: '/admin_panel',
            name: 'admin_panel',
            component: require('./components/AdminComponent.vue').default
        },
        {
            path: ':pedidoP/pedido',
            name: 'pedido',
            props: true,
            component: require('./components/PedidoComponent.vue').default
        },
        {
            path: '/pre_pedido',
            name: 'pre_pedido',
            component: require('./components/PrePedidoComponent.vue').default
        },
        {
            path: '/pagprincipal',
            name: 'pagprincipal',
            component: require('./components/PaginaPrincipal.vue').default
        },
        {
            path: '/register',
            name: 'register',
            component: require('./components/register.vue').default
        },
        {
            path: '/login',
            name: 'login',
            component: require('./components/login.vue').default
        },
        {
            path: '*',
            redirect: '/login'
        }
    ]

})

// router.beforeEach((to, from, next) => {
//     let usuario = !!localStorage.getItem('token');
//     let autorizacion = to.matched.some(record => record.meta.autenticado);

//     if (autorizacion && !usuario) {
//         next('login')
//     }else if(!autorizacion && usuario){
//         next('home');
//     }else{
//         next();
//     }
// })

export default router;