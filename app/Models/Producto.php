<?php

namespace App\Models;

use App\Models\User;
use App\Models\Pedido;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Producto extends Model
{
    use HasFactory;

    

    protected $fillable = [
        'nombre_producto',
        'imagen',
        'ingredientes',
        'user_id'
    ];

    public function productos(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function pedidos(){
        return $this->belongsToMany(Pedido::class);
    }


    
}
