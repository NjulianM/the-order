<?php

namespace App\Models;

use App\Models\Producto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pedido extends Model
{
    use HasFactory;

    const EN_ESPERA = 'espera';
    protected $fillable = [
        
        'estado',
        'empaque',
        'nombre_pedido'
    ];

    public function productos(){
        return $this->belongsToMany(Producto::class)->withPivot('cantidad','observaciones');;
    }

   
}
