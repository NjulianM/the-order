<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api')->except(['login','store']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos = $request->all();
        // if(!empty($request->image)){
            $campos['imagen']= $request->imagen->store('');
        // }else{
        //     $campos['imagen']= null;
        // }
       
        $campos['password'] = bcrypt($request->password);
        $usuario = User::create($campos);

        return $usuario;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
        $user=User::whereEmail($request->email)->first();
        if (!is_null($user)&& Hash::check($request->password, $user->password)) {
           $token= $user->createToken('usuarios')->accessToken;

           return response()->json([
               'res'=>true,
               'token'=>$token,
               'message'=>'token generado'
           ], 200);
        }
        else{
            return response()->json([
                'res'=>true,
                'message'=>'Error en autenticacion'
            ], 500);
        }
    }

    public function auth(Request $request)
    {
        $user = $request->user();
        
        return $user;
    }
}
